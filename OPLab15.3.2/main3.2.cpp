#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <windows.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));

	const int N = 5, M = 10;
	long arr[N][M];

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			arr[i][j] = -99 + rand() % 200;
			printf("%3d | ", arr[i][j]);
		}
		printf("\n");
	}

	for (int k = 0; k < N; k++) {
		long array[M];
		for (int i = 0; i < M; i++) array[i] = arr[k][i];

		long imin, c;
		for (int i = 0; i < M; i++) {
			imin = i;
			for (int j = i + 1; j < M; j++)
				if (array[j] < array[imin]) imin = j;
			c = array[i];
			array[i] = array[imin];
			array[imin] = c;
		}

		for (int i = 0; i < M; i++) arr[k][i] = array[i];
	}

	printf("\n\n��������� ���������� ������� ������: \n");
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			printf("%3d | ", arr[i][j]);
		}
		printf("\n");
	}


	return 0;
}