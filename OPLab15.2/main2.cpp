#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main() {
	srand(time(0));

	const int N = 10, M = 5;
	int arr[N][M];

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			arr[i][j] = -99 + rand() % 200;
			printf("%3d | ", arr[i][j]);
		}
		printf("\n");
	}

	for (int k = 0; k < M; k += 2) {
		for (int i = 0; i < N; i++) {
			if (arr[i][k] < 0) {
				for (int j = 0; j < N; j++) {
					if (arr[j][k] >= 0) {
						int c = arr[j][k];
						arr[j][k] = arr[i][k];
						arr[i][k] = c;
						break;
					}
				}
			}
		}
	}

	printf("\n");

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			printf("%3d | ", arr[i][j]);
		}
		printf("\n");
	}


	return 0;
}