#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main() {
	srand(time(0));

	const int N = 10;
	int arr[N][N];

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			arr[i][j] = -99 + rand() % 200;
			printf("%3d | ", arr[i][j]);
		}
		printf("\n");
	}

	for (int k = 1; k < N; k += 2) {
		int imin, a;
		for (int i = 0; i < N - 1; i++)
		{
			imin = i;
			for (int j = i + 1; j < N; j++)
				if (arr[j][k] < arr[imin][k]) imin = j;
			a = arr[i][k];
			arr[i][k] = arr[imin][k];
			arr[imin][k] = a;
		}
	}
	printf("\n\n\n");
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			printf("%3d | ", arr[i][j]);
		}
		printf("\n");
	}


	return 0;
}